const gulp = require('gulp')
const postcss = require('gulp-postcss')
const tailwindcss = require('tailwindcss')

const PATHS = {
    css: "./src/styles.css",
    html: "./src/*.html",
    config: "./tailwind.js",
    dist: "./dist"
}

gulp.task('html', () => {
    return gulp.src(PATHS.html)
        .pipe(gulp.dest(PATHS.dist));
});

gulp.task('css', () => {
    return gulp.src(PATHS.css)
        .pipe(postcss([
            tailwindcss(PATHS.config),
            require('autoprefixer')
        ]))
        .pipe(gulp.dest(PATHS.dist))
})

gulp.task('watch', () => {
    gulp.watch(['src/**/*.css'], ['css'])
    gulp.watch(['src/**/*.html'], ['html'])
})

gulp.task('clean', () => {
    return require('del')(['dist'])
})

gulp.task('serve', () => {
    return gulp.src(['./dist'])
        .pipe(
            require('gulp-server-io')()
        )
})

gulp.task('build', ['html', 'css'])

gulp.task('default', ['watch', 'serve'])